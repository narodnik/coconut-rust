use bls12_381 as bls;

use crate::bls_extensions::*;
use crate::parameters::*;

pub struct EncryptedValue {
    pub a: bls::G1Projective,
    pub b: bls::G1Projective
}

pub struct PrivateKey<'a, R: RngInstance> {
    params: &'a Parameters<R>,
    private_key: bls::Scalar
}

pub struct PublicKey<'a, R: RngInstance> {
    params: &'a Parameters<R>,
    pub public_key: bls::G1Projective
}

impl<'a, R: RngInstance> PrivateKey<'a, R> {
    pub fn new(params: &'a Parameters<R>) -> Self {
        Self {
            params: params,
            private_key: params.random_scalar()
        }
    }

    pub fn to_public(&self) -> PublicKey<'a, R> {
        PublicKey {
            params: self.params,
            public_key: self.params.g1 * self.private_key
        }
    }

    pub fn decrypt(&self, ciphertext: &EncryptedValue) -> bls::G1Projective {
        ciphertext.b - ciphertext.a * self.private_key
    }
}

impl<'a, R: RngInstance> PublicKey<'a, R> {
    pub fn encrypt(&self, attribute: &bls::Scalar, attribute_key: &bls::Scalar,
                   shared_value: &bls::G1Projective) -> EncryptedValue {
        EncryptedValue {
            a: self.params.g1 * attribute_key,
            b: self.public_key * attribute_key + shared_value * attribute
        }
    }
}

